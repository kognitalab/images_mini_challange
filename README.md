# images_mini_challange

## Introdução
Este repositório contém um mini-desafio voltado aos candidatos à vaga de Data Scientist (com foco em visão computacional) na Kognita Lab. Ele tem fim didático apenas, não consisitindo em nenhuma aplicação ou solução de mercado. 


## O desafio: 

Ao longo dos últimos anos observa-se um crescimento considerável no número de fotos produzidas utilizando-se os mais diversos dispositivos. Este fato demanda a criação e o aprimoramento de métodos, preferencialmente automáticos, para organizar e gerenciar tais coleções de imagens.  

Uma abordagem possível para esse problema consiste em, utilizando-se inteligência computacional, categorizar uma dada foto baseando-se na detecção do evento dominante que caracteriza tal imagem. Um caminho viável para alcançarmos este objetivo é por meio da utilização de técnicas de aprendizado estatístico supervisionado. Assim, dado um conjunto de imagens previamente rotuladas (training set), pode-se construir e empregar diversos tipos de algoritmos que aprendem e generalizam padrões. Desta forma, ao apresentarmos uma nova foto ao algoritmo previamente treinado, este estará apto a associá-la a uma das classes disponíveis.

No presente exercício, o objetivo é aplicar, de forma livre, tal idéia no dataset que apresentamos a seguir.


## O dataset:

Para este exercício usaremos um dataset simplificado baseado no dataset de imagens disponibilizado pelo Multimedia Signal Processing and Understanding Lab da Universidade de Trento, Itália (http://loki.disi.unitn.it/~used/).

O arquivo 'dataset_images_minitest.csv' (20987 linhas, baixável em http://bit.do/esYCR) contém as seguintes features:

i)  'filename': referência ao nome dos arquivos.   

ii) 'category': categoria (meeting, picnic ou graduation) a qual pertence uma dada imagem. 



Os arquivos de imagem estão contidos no arquivo 'data_folder.rar', que pode ser baixado (~900MB) por meio do seguinte link: http://bit.do/esYqT


## Regras:


Este exercício não tem como foco o resultado final (p. ex. a acurácia conseguida pelo candidato), mas sim observar a capacidade e a postura do candidato frente à problemas práticos. Neste sentido, pretende-se avaliar como o mesmo se propõe a investigar o problema, propor soluções, implementá-las e avaliá-las.

Seguem algumas orientações:

i) Ferramentas: Python, no Jupyter Notebook. Fique livre para escolher a(s) biblioteca(s) e framework(s) que preferir. Não é necessário reiventar ferramentas que já existem, mas espera-se que o candidato tenha alguma base teórica acerca do funcionamento dos algoritmos.  

ii) Neste exercício, dê preferência a construir seu código em apenas um arquivo .ipynb.  


iii) Comente e explique de forma clara seus passos e raciocínio. Justifique a escolha de seus modelos e dos caminhos que tomou ao longo do processo. Faça bom uso do Markdown.  

iv) Avalie e critique seus resultados.  

v) Seja criativo.  

vi) Divirta-se!  




Qualquer dúvida, entre em contato!